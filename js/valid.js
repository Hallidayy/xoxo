
const form = document.querySelector('.form')
const inputs = document.querySelectorAll('.input')
const btn = document.querySelector('.form_btn')
const addErr = document.querySelectorAll('.err_text_hide')
const login = form.elements.login
const password = form.elements.password



inputs.forEach(el=> el.addEventListener('input', function (e){
    if(login.value !== '' && password.value !== ''){
        btn.classList.add('btn_active')
    }else{
        btn.classList.remove('btn_active')
    }
    const reg = /[^\w\d_\.]/gi
    this.value = this.value.replaceAll(reg, "")
    this.oncopy =(e)=> e.preventDefault()
}))




form.addEventListener('submit', (event)=>{
    event.preventDefault()
    if(login.value === '' || password.value === ''){
        inputs.forEach(el=> el.classList.add('err_input'))
        addErr.forEach(el=> el.style.display = 'block')
        return false
    }else{
        inputs.forEach(el=> el.classList.remove('err_input'))
        addErr.forEach(el=> el.style.display = 'none')
    }
   console.log(`Логин: ${login.value}, пароль: ${password.value}`)
   event.target.reset()
})
