const whoWin = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

// получение элементов игрового поля
const field = document.querySelector(".game_field");
const cell = document.querySelectorAll(".game_field_cell");
const liveMotion = document.querySelector(".game_motion");
const modal = document.querySelector('.mod_container');
const modBtnRestart = document.getElementById('new_game');
let array = [...cell];


// очищение клеток игрового поля
cell.forEach((el) => (el.innerHTML = ""));
liveMotion.innerHTML = "";

let movesCross = [];
let movesZero = [];
let motion = "x";
let gameOver = false;

// вызов модального окна
const showModal = () => modal.style.display = 'block'


//функция для смены хода игрока
const changePlayer = () => (motion = motion == "x" ? "o" : "x");

// функция клика
const clickFunc = (event) => {
  event.preventDefault();
  const e = event.target;

  if (e.classList.contains("game_field_cell")) {
    let id = +e.id;
    if (motion === "x") {
      if (!cell[id].innerHTML) {
        movesCross.push(id);
        cell[id].innerHTML = '<img src="img/cross.svg">';
        liveMotion.innerHTML = 'Ходит <img src="icon/X.svg"> Вася Пупкин';
        checkWinner(movesCross);
        changePlayer();
      } else {
        alert("клетка занята");
      }
    } else if (motion === "o") {
      if (!cell[id].innerHTML) {
        movesZero.push(id);
        cell[id].innerHTML = '<img src="img/zero.svg">';
        liveMotion.innerHTML =
          'Ходит <img src="icon/zero.svg"> Плюшкина Екатерина';
        checkWinner(movesZero);
        changePlayer();
      } else {
        alert("клетка занята");
      }
    }
  }

  if (checkWinner(movesCross)) {
    liveMotion.innerHTML = 'Крестики <img src="icon/X.svg"> ПОБЕДИЛИ!';
    showModal()
  } else if (checkWinner(movesZero)) {
    liveMotion.innerHTML = 'Нолики <img src="icon/zero.svg"> ПОБЕДИЛИ!';
    showModal()
  } else if (array.every((el) => el.innerHTML !== "")) {
    liveMotion.innerHTML = "Ничья";
    showModal()
  }
};





// слушатель событий на игровом поле
field.addEventListener("click", clickFunc, false);



// функция поиска выигравшего игрока
const checkWinner = (player) => whoWin.some((el) => el.every((e) => player.includes(e)));


// функция рестарта игрового поля
const restart = () => {
  field.removeEventListener("click", clickFunc, false);
  modal.style.display = 'none'
  setTimeout(() => {
    field.addEventListener("click", clickFunc, false);
    cell.forEach((el) => (el.innerHTML = ""));
    liveMotion.innerHTML = "";
    movesCross = [];
    movesZero = [];
    motion = "x";
  }, 2000);
};

//слушатель событий модалки
modBtnRestart.addEventListener('click', restart, false);